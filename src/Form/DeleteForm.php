<?php

declare(strict_types = 1);

namespace Drupal\rdf_meta_entity\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\rdf_entity\Form\RdfDeleteForm;

/**
 * Provides a form for deleting an rdf_meta_entity.
 */
class DeleteForm extends RdfDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): string {
    $graph = $this->entity->get('graph')->target_id;
    $target_entity = $this->entity->getTargetEntity();
    return $this->t('Are you sure you want to delete the RDF meta entity of the %target_name from the graph %graph?', [
      '%target_name' => $target_entity->label(),
      '%graph' => $graph,
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and navigate to the target entity.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\rdf_meta_entity\Entity\RdfMetaEntityInterface $entity */
    $entity = $this->getEntity();
    $target_entity = $entity->getTargetEntity();
    $entity->delete();

    $this->logger('rdf_meta_entity')->notice('@type: deleted %title.', [
      '@type' => $this->entity->bundle(),
      '%title' => $this->entity->label(),
    ]);

    if (empty($form_state->getRedirect())) {
      $form_state->setRedirectUrl($target_entity->toUrl());
    }
  }

}
