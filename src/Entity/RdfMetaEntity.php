<?php

declare(strict_types = 1);

namespace Drupal\rdf_meta_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\meta_entity\Entity\MetaEntity;
use Drupal\meta_entity\Entity\MetaEntityInterface;

/**
 * Defines the meta entity content entity class with SPARQL storage backend.
 *
 * @ContentEntityType(
 *   id = "rdf_meta_entity",
 *   label = @Translation("RDF meta entity"),
 *   label_collection = @Translation("RDF meta entities"),
 *   bundle_label = @Translation("RDF meta entity type"),
 *   handlers = {
 *     "access" = "Drupal\meta_entity\MetaEntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\meta_entity\Form\MetaEntityForm",
 *       "add" = "Drupal\meta_entity\Form\MetaEntityForm",
 *       "delete" = "\Drupal\rdf_meta_entity\Form\DeleteForm",
 *     },
 *     "list_builder" = "Drupal\meta_entity\MetaEntityListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "storage" = "\Drupal\sparql_entity_storage\SparqlEntityStorage",
 *   },
 *   links = {
 *     "add-form" = "/admin/content/rdf-meta-entity/add/{rdf_meta_entity_type}",
 *     "add-page" = "/admin/content/rdf-meta-entity/add",
 *     "canonical" = "/rdf-meta-entity/{rdf_meta_entity}",
 *     "edit-form" = "/admin/content/rdf-meta-entity/{rdf_meta_entity}/edit",
 *     "delete-form" = "/admin/content/rdf-meta-entity/{rdf_meta_entity}/delete",
 *     "collection" = "/admin/content/rdf-meta-entity"
 *   },
 *   bundle_entity_type = "rdf_meta_entity_type",
 *   admin_permission = "administer rdf meta entity",
 *   field_ui_base_route = "entity.rdf_meta_entity_type.edit_form",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 * )
 */
class RdfMetaEntity extends MetaEntity implements RdfMetaEntityInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    return [
      'id' => BaseFieldDefinition::create('uri')
        ->setLabel(t('ID'))
        ->setTranslatable(FALSE),
    ] + parent::baseFieldDefinitions($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public static function loadOrCreate(string $meta_entity_bundle, ContentEntityInterface $target_entity): MetaEntityInterface {
    return self::loadOrCreateHelper('rdf_meta_entity', $meta_entity_bundle, $target_entity);
  }

}
