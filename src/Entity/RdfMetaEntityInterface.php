<?php

declare(strict_types = 1);

namespace Drupal\rdf_meta_entity\Entity;

use Drupal\meta_entity\Entity\MetaEntityInterface;

/**
 * Provides an interface for RDF Meta Entity.
 */
interface RdfMetaEntityInterface extends MetaEntityInterface {}
