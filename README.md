# RDF Meta Entity

Allows storing metadata about an entity in entities having a SPARQL backend. The
module is an extension of [Meta Entity](https://www.drupal.org/project/meta_entity
) which uses the entity storage provided by [SPARQL Entity
Storage](https://www.drupal.org/project/sparql_entity_storage) module.
